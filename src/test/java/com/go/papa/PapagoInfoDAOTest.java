//
//package com.go.papa;
//
//import java.util.List;
//
//import javax.annotation.Resource;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.util.Assert;
//
//import com.go.papa.dao.PapagoInfoDAO;
//import com.go.papa.vo.PapagoInfoVO;
//
//import lombok.extern.slf4j.Slf4j;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//
//@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/root-context.xml" })
//
//@Slf4j
//public class PapagoInfoDAOTest {
//
//	@Resource
//	private PapagoInfoDAO pidao;
//
//	@Test
//	public void test() {
//		Assert.notNull(pidao, "그럼 그렇지");
//		List<PapagoInfoVO> piList = pidao.selectPapagoInfoList(null);
//		Assert.isTrue(piList.size() == 1, "아 맞아 목록 1개였지");
//	}
//
//}
