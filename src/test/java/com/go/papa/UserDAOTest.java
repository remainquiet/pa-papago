//package com.go.papa;
//
//import javax.annotation.Resource;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.util.Assert;
//
//import com.go.papa.dao.UserDAO;
//import com.go.papa.vo.UserInfoVO;
//
//import lombok.extern.slf4j.Slf4j;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations= {
//		"classpath:root-context.xml"
//})
//@Slf4j
//public class UserDAOTest {
//
//	@Resource
//	private UserDAO udao;
//	
//	@Test
//	public void test() {
//		UserInfoVO ui = new UserInfoVO();
//		ui.setUiId("dozero");
//		ui.setUiPwd("123456");
//		ui = udao.doLogin(ui);
//		log.info("ui=>{}",ui);
//		Assert.notNull(ui,"로그인 성공");
//	}
//
//}
