<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<script src="http://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
<script src="/res/js/ajax.js"></script>
<body>
<div id="wrap-container">
	<div class="buttons">
		<button data-target="all">전체</button>
		<button data-target="credat">일자별</button>
		<button data-target="ui_num">유저별</button>
	</div>
</div>
	<table border="1" data-id="all">
		<tr>
			<th data-order="ps_num asc">기록번호▲</th>
			<th data-order="ps.ui_num desc">번역요청자번호▼</th>
			<th data-order="ui_id desc">번역요청자아이디▼</th>
			<th data-order="pi_num desc">변역번호▼</th>
			<th data-order="ps.credat desc">번역 일자▼</th>
		</tr>
		<tbody id="tBody">
		</tbody>
	</table>
	
	<table border="1" data-id="credat" style="display:none">
		<tr>
			<th>일자</th>
			<th>번역횟수</th>
		</tr>
		<tbody id="cBody">
		</tbody>
	</table>
	
	<table border="1" data-id="ui_num" style="display:none">
		<tr>
			<th>번역요청자번호</th>
			<th>번역번호</th>
			<th>번역요청자이름</th>
			<th>번역횟수</th>
		</tr>
		<tbody id="uBody">
		</tbody>
	</table>
	<script>

$(document).ready(function(){
	$('button[data-target]').on('click',function(){
		$('table[data-id]').css('display','none');
		$('table[data-id=' + this.getAttribute('data-target') + ']').css('display','');
	})
	$('th[data-order]').on('click',function(){
		var text = this.innerText;
		var symbol = text.substring(text.length-1,text.length);
		if(symbol=='▼'){
			this.innerText = text.substring(0,text.length-1) + '▲';
			this.setAttribute('data-order',this.getAttribute('data-order').substring(0,this.getAttribute('data-order').length-4) + ' desc');
		}else{
			this.innerText = text.substring(0,text.length-1) + '▼';
			this.setAttribute('data-order',this.getAttribute('data-order').substring(0,this.getAttribute('data-order').length-4) + ' asc');
		}
		var param = 'order=' + this.getAttribute('data-order');
		dataLoad(param);
	})
	dataLoad();
	userdataLoad();
	credataLoad();
})
	function dataLoad(data){
	var html = '';
	ajax({
		url : '/papago/stats',
		method : 'GET',
		data : data,
		success : function(res){
			console.log(res);
			for(var idx of res){
				html += '<tr>';
				html += '<td>' + idx.psNum + '</td>';
				html += '<td>' + idx.uiNum + '</td>';
				html += '<td>' + idx.uiId + '</td>';
				html += '<td>' + idx.piNum + '</td>';
				html += '<td>' + idx.credat + '</td>';
				html += '</tr>';
			}
			$('#tBody').html(html);
		},
		error : function(res){
			console.log(res);
		}
	})
}

function credataLoad(data){
	var html = '';
	ajax({
		url : '/papago/stats/credat',
		method : 'GET',
		data : data,
		success : function(res){
			console.log(res);
			for(var idx of res){
				html += '<tr>';
				html += '<td>' + idx.credat + '</td>';
				html += '<td>' + idx.cnt + '</td>';
				html += '</tr>';
			}
			$('#cBody').html(html);
		},
		error : function(res){
			console.log(res);
		}
	})
}

function userdataLoad(data){
	var html = '';
	ajax({
		url : '/papago/stats/user',
		method : 'GET',
		data : data,
		success : function(res){
			console.log(res);
			for(var idx of res){
				html += '<tr>';
				html += '<td>' + idx.uiNum + '</td>';
				html += '<td>' + idx.uiId + '</td>';
				html += '<td>' + idx.piNum + '</td>';
				html += '<td>' + idx.cnt + '</td>';
				html += '</tr>';
			}
			$('#uBody').html(html);
		},
		error : function(res){
			console.log(res);
		}
	})
}
</script>
</body>
</html>