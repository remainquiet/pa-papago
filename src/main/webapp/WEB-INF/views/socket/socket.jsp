<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<button id="open">연결</button>
<div id = "rDiv" style="display:none">
	<input type="text" id="msg"><button id="send">전송</button>
</div>
<script>
window.onload = function(){
	document.querySelector('button').onclick = function(){
		var url = 'ws://192.168.0.46/ws/chat';
		var ws = new WebSocket(url);
		
		ws.onopen = function(evt){
			console.log(evt);
			if(evt && evt.isTrusted){
				document.querySelector('#open').style.display = 'none';
				document.querySelector('#rDiv').style.display = '';
				document.querySelector('#send').onclick = function(){
					var msg = document.querySelector('#msg').value;
					ws.send(msg);
				}
			}
		}
		ws.onmessage = function(evt){
			alert(evt.data);
		}
	}
}
</script>
</body>
</html>