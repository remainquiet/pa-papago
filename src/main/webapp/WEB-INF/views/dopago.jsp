<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>파파고 번역기</title>
<jsp:include page="/WEB-INF/views/common/head.jsp" />
<style>
textarea {
	width: 30%;
	height: 300px;
	padding: 10px;
	box-sizing: border-box;
	border: solid 2px rgb(60, 226, 101);
	border-radius: 5px;
	font-size: 16px;
	resize: both;
}

select {
	width: 10%;
	height: 30px;
	font: icon;
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
	border: 1px solid rgb(100, 100, 100);
}

button {
	width: 50px;
	height: 30px;
	border: 1px solid rgb(60, 226, 101);
	background-color: rgb(60, 226, 101);
	color: rgb(255, 255, 255);
	padding: 5px;
}
</style>
</head>
<body>
	<div class="tra">
		<div class="header">
			<h1 align="center">Papago</h1>
		</div>
		<div class="header"></div>
		<div class="selectBox1">
			<select id="source" class="selectLang">
				<option value="ko">한국어</option>
				<option value="en">영어</option>
				<option value="ja">일본어</option>
				<option value="zh-CN">중국어(간체)</option>
				<option value="zh-TW">중국어(번체)</option>
				<option value="es">스페인어</option>
				<option value="fr">프랑스어</option>
				<option value="de">독일어</option>
				<option value="ru">러시아어</option>
				<option value="it">이탈리아어</option>
				<option value="vi">베트남어</option>
				<option value="th">태국어</option>
				<option value="id">인도네시아어</option>
			</select>
		</div>
		<textarea id="text" placeholder="내용을 입력해주세요" required></textarea>
		<button type="button" onclick="doTranslate()">번역</button>
		<div class="selectBox2">
			<select id="target" class="selectLang">
				<option value="ko">한국어</option>
				<option value="en">영어</option>
				<option value="ja">일본어</option>
				<option value="zh-CN">중국어(간체)</option>
				<option value="zh-TW">중국어(번체)</option>
				<option value="es">스페인어</option>
				<option value="fr">프랑스어</option>
				<option value="de">독일어</option>
				<option value="ru">러시아어</option>
				<option value="it">이탈리아어</option>
				<option value="vi">베트남어</option>
				<option value="th">태국어</option>
				<option value="id">인도네시아어</option>
			</select>
		</div>
		<textarea readonly id="translatedText"></textarea>
		<div class="footer"></div>
	</div>
	<script>
		function doTranslate() {
			var xhr = new XMLHttpRequest();
			xhr.open('POST', '/papago');
			xhr.setRequestHeader('Content-Type', 'application/json');
			var param = {
				text : document.querySelector('#text').value,
				source : document.querySelector('#source').value,
				target : document.querySelector('#target').value
			}
			console.log(param);
			xhr.onreadystatechange = function() {
				if (xhr.readyState == xhr.DONE) {
					if (xhr.status == 200) {
						var text = JSON.parse(xhr.responseText);
						document.querySelector('#translatedText').value = text.result.translatedText;
					}
				}
			}
			xhr.send(JSON.stringify(param));
		}
	</script>
</body>
</html>