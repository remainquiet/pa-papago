<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<script src="http://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
<script src="/res/js/ajax.js"></script>
<body>
<h1>PapagoStatsInfo List</h1>
<table border="1">
<tr>
	<th>번호</th>
	<th>유저 번호</th>
	<th>번역된 번호</th>
	<th>생성일</th>
</tr>
<tbody id="tBody">
</tbody>
</table>
<button type="button" onclick="location.href='/views/papago/papago'">돌아가기</button>
<script>
$(document).ready(function(){
	var param = {
			psNum : ''
	}
	var html = '';
		var conf = {
				url : '/selectPapagoStatsInfo',
				method : 'POST',
				data : param,
				success : function(res){
					for(var i in res){
					html += '<tr>';
					html += '<td>' + res[i].psNum + '</td>';
					html += '<td>' + res[i].uiNum + '</td>';
					html += '<td>' + res[i].piNum + '</td>';
					html += '<td>' + res[i].credat + '</td>';
					html += '</tr>';	
					}
					$('#tBody').html(html);
				},
				error : function(res){
					console.log(res);
				}
			}
		ajax(conf);
	});
</script>
</body>
</html>