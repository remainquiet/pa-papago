<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<title>Login V11</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="/res/login/image/png" href="/res/login/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/res/login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/res/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/res/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/res/login/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/res/login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/res/login/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/res/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="/res/login/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-30">
				<form class="login100-form validate-form">
					<span class="login100-form-title p-b-55">
						회원 가입
					</span>

					<div class="wrap-input100 validate-input m-b-16" data-validate = "이메일 형식이 아닙니다. Ex) abc@naver.com">
						<input class="input100" type="text" name="email" placeholder="Email" id="uiId">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-envelope"></span>
						</span>
					</div>

					<div class="wrap-input100 validate-input m-b-16" data-validate = "특수문자를 최소 1개 이상 포함해야 합니다">
						<input class="input100" type="password" name="pass" placeholder="Password" id="uiPwd">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-lock"></span>
						</span>
					</div>
					
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
						<input class="input100" type="password" name="pass" placeholder="Password Check" id="uiPwdCheck">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-lock"></span>
						</span>
					</div>
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Name is required">
						<input class="input100" type="text" name="name" placeholder="name" id="uiName">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-user"></span>
						</span>
					</div>					
					
					
					<div class="container-login100-form-btn p-t-25">
						<button class="login100-form-btn">
							Sign Up
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="/res/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="/res/login/vendor/bootstrap/js/popper.js"></script>
	<script src="/res/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="/res/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="/res/login/js/main.js"></script>
<script>
$(document).ready(function(){
	
	$('button').on('click',function(){
		var param = {
				uiId : $('#uiId').val(),
				uiPwd : document.querySelector('#uiPwd').value
		}
		param = JSON.stringify(param);
		$.ajax({
			url : '/login',
			method : 'POST',
			data : param,
			contentType : 'application/json',
			success : function(res){
				if(res.msg){
				alert(res.msg);
				}
			},
			error : function(res){
				alert(res.msg);
			}
		})
	})
	
})
</script>
</body>
</html>