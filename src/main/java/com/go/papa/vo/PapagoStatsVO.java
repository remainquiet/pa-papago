package com.go.papa.vo;


public class PapagoStatsVO {

	private Integer psNum;
	private Integer uiNum;
	private Integer piNum;
	private String credat;
	private String order;
	private String uiId;
	private String cnt;
	
	public String getCnt() {
		return cnt;
	}
	public void setCnt(String cnt) {
		this.cnt = cnt;
	}
	public String getUiId() {
		return uiId;
	}
	public void setUiId(String uiId) {
		this.uiId = uiId;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public Integer getPsNum() {
		return psNum;
	}
	public void setPsNum(Integer psNum) {
		this.psNum = psNum;
	}
	public Integer getUiNum() {
		return uiNum;
	}
	public void setUiNum(Integer uiNum) {
		this.uiNum = uiNum;
	}
	public Integer getPiNum() {
		return piNum;
	}
	public void setPiNum(Integer piNum) {
		this.piNum = piNum;
	}
	public String getCredat() {
		return credat;
	}
	public void setCredat(String credat) {
		this.credat = credat;
	}
	@Override
	public String toString() {
		return "PapagoStatsVO [psNum=" + psNum + ", uiNum=" + uiNum + ", piNum=" + piNum + ", credat=" + credat
				+ ", order=" + order + ", uiId=" + uiId + ", cnt=" + cnt + "]";
	}
	
}
