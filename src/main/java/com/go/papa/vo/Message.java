package com.go.papa.vo;

import lombok.Data;

@Data
public class Message {

	private Result result;
}
