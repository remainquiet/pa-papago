package com.go.papa.service;

import java.util.List;
import java.util.Map;

import com.go.papa.vo.PapagoStatsVO;

public interface PapagoStatsService {

	public List<PapagoStatsVO> selectPapagoStatss(PapagoStatsVO ps);
	public Map<String,String> insertPapagoStats(PapagoStatsVO ps);
	public List<PapagoStatsVO> selectPapagoStatsCredat(PapagoStatsVO ps);
	public List<PapagoStatsVO> selectPapagoStatsUser(PapagoStatsVO ps);
}
