package com.go.papa.service;

import java.util.List;

import com.go.papa.vo.Message;
import com.go.papa.vo.PapagoInfoVO;
import com.go.papa.vo.TransVO;

public interface PapagoService {

	public Message doTranslate(TransVO tvo);
	public List<PapagoInfoVO> getPapagoInfoList(PapagoInfoVO pi);
}
