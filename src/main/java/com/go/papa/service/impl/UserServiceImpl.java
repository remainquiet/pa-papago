package com.go.papa.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.go.papa.dao.UserDAO;
import com.go.papa.service.UserService;
import com.go.papa.vo.UserInfoVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
	
	@Resource
	private UserDAO udao;


	@Override
	public Map<String, Object> doLogin(UserInfoVO ui, HttpSession hs) {
		ui = udao.doLogin(ui);
		log.info("ui=>{}",ui);
		Map<String,Object> rMap = new HashMap<>();
		if(ui==null) {
			rMap.put("msg", "아이디나 비밀번호를 확인해주세요");
		}else {
			rMap.put("msg", "로그인 성공");
			rMap.put("url", "/views/socket/test");
			rMap.put("ui",ui);
			hs.setAttribute("ui", ui);
			log.info(hs.getId());
			log.info(""+hs.getAttribute("ui"));
		}
		return rMap;
	}

}
