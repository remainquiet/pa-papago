package com.go.papa.service.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import javax.annotation.Resource;
import javax.net.ssl.HttpsURLConnection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.go.papa.dao.PapagoInfoDAO;
import com.go.papa.dao.PapagoStatsDAO;
import com.go.papa.service.PapagoService;
import com.go.papa.vo.Message;
import com.go.papa.vo.PapagoInfoVO;
import com.go.papa.vo.PapagoStatsVO;
import com.go.papa.vo.Result;
import com.go.papa.vo.TransVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PapagoServiceImpl implements PapagoService {

	@Resource
	private PapagoInfoDAO pdao;
	@Resource
	private PapagoStatsDAO psdao;
	
	@Value("${client.id}")
	private String id;

	@Value("${client.secret}")
	private String secret;

	@Value("${client.api.url}")
	private String apiurl;

	private ObjectMapper om = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);//없는 키값을 무시하라
	@Override
	public Message doTranslate(TransVO tvo) {
		
		try {
			PapagoStatsVO ps = new PapagoStatsVO();
			PapagoInfoVO pi = new PapagoInfoVO();
			
			ps.setUiNum(tvo.getUiNum());
			pi.setPiText(tvo.getText());
			pi.setPiSource(tvo.getSource());
			pi.setPiTarget(tvo.getTarget());
			pi = pdao.selectPapagoInfo(pi);
			if(pi!=null) {					
				Result r = new Result();
				r.setSrcLangType(pi.getPiSource());
				r.setTarLangType(pi.getPiTarget());
				r.setTranslatedText(pi.getPiResult());
				Message m = new Message();
				m.setResult(r);
				pdao.updatePapagoInfoForCnt(pi);
				ps.setPiNum(pi.getPiNum());
				psdao.insertPapagoStats(ps);
				return m;
			}
			
			String text = URLEncoder.encode(tvo.getText(), "UTF-8");
			URL url = new URL(apiurl);// url 형태인지 체크하는부분
			HttpsURLConnection hc = (HttpsURLConnection) url.openConnection();// 실제로 연결하는 부분은 이쪽 url이 잘못되었다면
			hc.setRequestMethod("POST");
			hc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			hc.setRequestProperty("X-Naver-Client-Id", id);
			hc.setRequestProperty("X-Naver-Client-Secret", secret);
			hc.setDoOutput(true);//true가 되어야 파라미터를 보낼 수 있음 default가 false다 
			String param = "source="+ tvo.getSource() + "&target=" + tvo.getTarget() + "&text=" + text;
			DataOutputStream dos = new DataOutputStream(hc.getOutputStream());// 연결은 그대로 있고 얘만 close
			dos.writeBytes(param);
			dos.flush();
			dos.close();

			int status = hc.getResponseCode();
			InputStreamReader isr = new InputStreamReader(hc.getInputStream());
			BufferedReader br = new BufferedReader(isr);
			StringBuffer sb = new StringBuffer();
			String str = null;
			while ((str = br.readLine()) != null) {
				sb.append(str);
			}
			br.close();
			TransVO resultVO = om.readValue(sb.toString(),TransVO.class);
			Result r = resultVO.getMessage().getResult();
			pi = new PapagoInfoVO();
			pi.setPiSource(r.getSrcLangType());
			pi.setPiTarget(r.getTarLangType());
			pi.setPiResult(r.getTranslatedText());
			pi.setPiText(tvo.getText());
			pdao.insertPapagoInfo(pi);
			ps.setPiNum(pi.getPiNum());
			psdao.insertPapagoStats(ps);
			return resultVO.getMessage();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();

		}
		return null;
	}
	@Override
	public List<PapagoInfoVO> getPapagoInfoList(PapagoInfoVO pi) {
		return pdao.selectPapagoInfoList(pi);
	}

}
