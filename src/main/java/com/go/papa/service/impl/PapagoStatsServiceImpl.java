package com.go.papa.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.go.papa.dao.PapagoStatsDAO;
import com.go.papa.service.PapagoStatsService;
import com.go.papa.vo.PapagoStatsVO;

@Service
public class PapagoStatsServiceImpl implements PapagoStatsService {

	@Resource
	private PapagoStatsDAO psdao;
	
	@Override
	public List<PapagoStatsVO> selectPapagoStatss(PapagoStatsVO ps) {
		return psdao.selectPapagoStatss(ps);
	}

	@Override
	public Map<String, String> insertPapagoStats(PapagoStatsVO ps) {
		Map<String,String> map = new HashMap<>();
		map.put("msg", "실패");
		int cnt = psdao.insertPapagoStats(ps);
		if(cnt==1) {
			map.put("msg", "성공");
		}
		map.put("cnt",""+cnt);
		return map;
	}

	@Override
	public List<PapagoStatsVO> selectPapagoStatsCredat(PapagoStatsVO ps) {
		return psdao.selectPapagoStatsCredat(ps);
	}

	@Override
	public List<PapagoStatsVO> selectPapagoStatsUser(PapagoStatsVO ps) {
		return psdao.selectPapagoStatsUser(ps);
	}

}
