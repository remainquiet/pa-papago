package com.go.papa.service;

import java.util.Map;

import javax.servlet.http.HttpSession;

import com.go.papa.vo.UserInfoVO;

public interface UserService {

	public Map<String,Object> doLogin(UserInfoVO ui, HttpSession hs);
}
