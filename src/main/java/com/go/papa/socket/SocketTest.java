package com.go.papa.socket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

//@ServerEndpoint(value="/ws/chat", configurator = ServletConfig.class) // websocket 프로토콜이라 ws를 써야한다 ws://localhost//~~이런식
@Slf4j
public class SocketTest {
	
	private static List<Session> ssList = new ArrayList<>();
	private static Map<Session, String> ssMap = new HashMap<>();

	@OnOpen // 해당 서버에 연결을 했을때 ws = new WebSocket(url);이 때 연결
	public void open(Session ss, EndpointConfig epc) {// HttpSession이 아니다 Websocket Session임
		HttpSession hs = (HttpSession) epc.getUserProperties().get("hs");
		
		log.info("httpSession=>{}",hs);
		log.info("ServletContext=>{}",ss);

		if (ssList.indexOf(ss) == -1) {
			ssList.add(ss);
		}
		log.info(ss.getId());
		try {
			ss.getBasicRemote()
					.sendText("랜덤한 모르는 사람과 채팅을 즐기시길 바랍니다!\r\n" + "[사칭주의] 관리자나 사이버수사대라고 하는 경우는 모두 사칭입니다.\r\n"
							+ "[사기주의] 특정 주소를 쳐서 접속하라는 성인 사이트는 모두 사기입니다.\r\n"
							+ "[사기주의] 대화 없이 카톡/라인/스카이프 아이디를 뿌리는 경우 모두 사기입니다.\r\n"
							+ "타인을 사칭하는 경우 별도로 법적 제재 가능한 점 참고 부탁드립니다.\r\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@OnMessage // 클라이언트로부터 Message가 들어왔을 때
	public void message(String msg, Session ss) {
		log.info("msg=>{}", msg);
		List<String> idList = new ArrayList<>();
		idList.add(msg);
		for (Session ss1 : ssList) {
			try {
				ss1.getBasicRemote().sendText("\r" + ss.getId() + "번째 손님 :" + msg);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	@OnClose
	public void close(Session ss) {
		ssList.remove(ss);
		log.info("닫혔다면 출력함 해봐라=>{}", ss);
		log.info(ss.getId());
	}

	@OnError
	public void error(Throwable t) {
		log.error("err=>{}" + t);
	}
}
