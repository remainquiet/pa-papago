package com.go.papa.socket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.go.papa.dao.MessageInfoDAO;
import com.go.papa.dao.impl.MessageInfoDAOImpl;
import com.go.papa.vo.MessageInfoVO;
import com.go.papa.vo.UserInfoVO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WebSocketTest  extends TextWebSocketHandler{
	private static List<WebSocketSession> ssList = new ArrayList<>();

	@Resource
	MessageInfoDAO midao;
	
	MessageInfoVO mivo= new MessageInfoVO();
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		Map<String,Object> attrs = session.getAttributes();
		HttpSession hs = (HttpSession)attrs.get("session");
		log.info("id==>{}",hs.getId());
		String msg = "";
		if(hs.getAttribute("ui")!=null) {
		UserInfoVO uvo = (UserInfoVO) hs.getAttribute("ui");
		msg = uvo.getUiName() + "님 환영합니다";
		}
		else msg = "손님 : " + "환영합니다";
		
		log.info("webSession==>{}", session.getId());
		log.info("HttpSession==>{}", hs.getId());
		if(ssList.indexOf(session)==-1) {
			ssList.add(session);
		}
		try {
			session.sendMessage(new TextMessage(msg));
			//Session.sendMessage(new TextMessage(entity.toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		Map<String,Object> attrs = session.getAttributes();
		HttpSession hs = (HttpSession)attrs.get("session");
		String msg = "";
		if(hs.getAttribute("ui")!=null) {
		UserInfoVO uvo = (UserInfoVO) hs.getAttribute("ui");
		mivo.setUiName(uvo.getUiName());
		mivo.setMiMsg(message.getPayload());
		midao.insertMessageInfo(mivo);
		msg = "\r\n" + uvo.getUiName() + " 님 : "+ message.getPayload();
		}
		else {
			msg = "\r\n손님 : " + message.getPayload();
			mivo.setUiName("손님");
			mivo.setMiMsg(message.getPayload());
			midao.insertMessageInfo(mivo);
		}
		for (WebSocketSession ss1 : ssList) {
			try {
				ss1.sendMessage(new TextMessage(msg));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		ssList.remove(session);
	}
	@Override
	protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message){
		log.info("message=>{}",message);
		
	}
	
	
}
