package com.go.papa.test;

public class dsada {

	public String solution(String[] participant, String[] completion) {
		String answer = "";
		for (int i = 0; i < completion.length; i++) {
			for (int j = 0; j < participant.length; j++) {
				if (completion[i].indexOf(participant[j]) == -1) {
					answer = participant[j];
				}
			}

		}
		return answer;
	}

	public static void main(String[] args) {
		dsada s = new dsada();
		String[] participant = { "leo", "kiki", "eden" };
		String[] completion = { "eden", "kiki" };
		s.solution(participant, completion);
	}
}
