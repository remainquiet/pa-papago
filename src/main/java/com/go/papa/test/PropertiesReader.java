package com.go.papa.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {

	public static void main(String[] args) {
		String path = "test.properties";
		try {
			InputStream fis = PropertiesReader.class.getClassLoader().getResourceAsStream(path);
			Properties prop = new Properties();
			prop.load(fis);
			System.out.println(prop);
			System.out.println(prop.getProperty("client.secret"));
			System.out.println(prop.getProperty("client.id"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
