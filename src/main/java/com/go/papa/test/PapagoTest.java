package com.go.papa.test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class PapagoTest {
	
	@Value("${client.id}")
	private String id;

	@Value("${client.secret}")
	private String secret;
	
	@Value("${client.api.url}")
	private String apiurl;

	public void doPapagoTest() {
		try {
			String text = URLEncoder.encode("동민아, 일찍와라","UTF-8");
			URL url = new URL(apiurl);//url 형태인지 체크하는부분
			HttpsURLConnection hc = (HttpsURLConnection) url.openConnection();//실제로 연결하는 부분은 이쪽 url이 잘못되었다면 
			hc.setRequestMethod("POST");
			hc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			hc.setRequestProperty("X-Naver-Client-Id", id);
			hc.setRequestProperty("X-Naver-Client-Secret", secret);
			hc.setDoOutput(true);
			String param = "source=ko&target=en&text=" + text;
			DataOutputStream dos = new DataOutputStream(hc.getOutputStream());//연결은 그대로 있고 얘만 close
			dos.writeBytes(param);
			dos.flush();
			dos.close();
			
			int status = hc.getResponseCode();
			if(status!=200) {
				System.out.println("에러");
				return;
			}
			InputStreamReader isr = new InputStreamReader(hc.getInputStream());
			BufferedReader br = new BufferedReader(isr);
			String res = "";
			String str = null;
			while((str=br.readLine())!=null) {
				res += str;
			}
			System.out.println(res);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();

		}
	}
	public static void main(String[] args) {
		PapagoTest pt = new PapagoTest();
		pt.doPapagoTest();
		System.out.println(pt.getId());
		
	}
}
