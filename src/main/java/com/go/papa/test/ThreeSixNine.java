package com.go.papa.test;

public class ThreeSixNine {

	public static void main(String[] args) {

		for (int i = 1; i <= 100; i++) {

			String str = i + "";

			if (str.indexOf("3") != -1 || str.indexOf("6") != -1 || str.indexOf("9") != -1) {
				str = str.replaceAll("3", "짝");
				str = str.replaceAll("6", "짝");
				str = str.replaceAll("9", "짝");
			}

			try {
				System.out.println(Integer.parseInt(str));
			} catch (Exception e) {
				if (!str.equals("짝짝")) {
					System.out.println("짝");
				}else {
					System.out.println(str);
				}
			}
		}
	}
}