package com.go.papa.interceptor;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Component("si")
@Slf4j
public class SessionInterceptor extends HandlerInterceptorAdapter{//아직 메모리를 생성하지 않았다

	private ObjectMapper om = new ObjectMapper();
	
	public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler) throws IOException {
		String uri = req.getRequestURI();
		log.info("uri=>{}",uri);
		if(req.getSession().getAttribute("ui")!=null) {
			return true;
		}else {
			String isAjax = req.getHeader("x-ajax");
			log.info("isAjax=>{}",isAjax);
			if(isAjax!=null) {
				Map<String,String> map = new HashMap<>();
				map.put("msg", "로그인 안됨");
				res.setContentType("application/json;charset=utf-8");
				PrintWriter pw = res.getWriter();
				pw.write(om.writeValueAsString(map));
				return false;
			}
			res.sendRedirect("/views/error/session-error");
			return false;
		}
	}
}
