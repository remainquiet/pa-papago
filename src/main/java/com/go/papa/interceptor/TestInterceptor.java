package com.go.papa.interceptor;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.go.papa.service.UserService;
import com.go.papa.vo.UserInfoVO;


@Component("ti")
public class TestInterceptor extends HandlerInterceptorAdapter {

	private ObjectMapper om = new ObjectMapper();
	@Resource
	private UserService us;
	@Value("${project.mod}")
	private String projectMod;

	public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler) throws IOException {
		HttpSession hs = req.getSession();
		if (hs.getAttribute("ui") == null) {
			if ("DEV".equals(projectMod)) {
				UserInfoVO ui = new UserInfoVO();
				ui.setUiId("dozero");
				ui.setUiPwd("123456");
				us.doLogin(ui, hs);
				return true;
			}
		}

		if (req.getSession().getAttribute("ui") == null) {
			if (req.getHeader("x-ajax") != null) {
				Map<String, String> map = new HashMap<>();
				map.put("msg", "로그인 해주세요");
				res.setContentType("application/json;charset=utf-8");
				PrintWriter pw = res.getWriter();
				pw.write(om.writeValueAsString(map));
				return false;
			}
			res.sendRedirect("/views/error/session-error");
			return false;
		} else {
			return true;
		}
	}
}
