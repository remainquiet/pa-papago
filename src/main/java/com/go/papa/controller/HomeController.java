package com.go.papa.controller;

import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.go.papa.test.PapagoTest;

import lombok.extern.slf4j.Slf4j;

/**
 * Handles requests for the application home page.
 */
@Controller
@Slf4j
public class HomeController {
	
	@Resource
	private PapagoTest pt;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		
		return "user/login2";
	}
	
	@GetMapping("/views/**")
	public String goPage(HttpServletRequest req) {
		String uri = req.getRequestURI();
		log.info("uri=>{}",uri.substring(7));
		return uri.substring(7);
	}
	
}
