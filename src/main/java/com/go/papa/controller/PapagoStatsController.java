package com.go.papa.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.go.papa.service.PapagoStatsService;
import com.go.papa.vo.PapagoStatsVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class PapagoStatsController {

	@Resource
	private PapagoStatsService pss;
	
	@GetMapping("/papago/stats")
	public List<PapagoStatsVO> getPapagoStatsList(PapagoStatsVO ps){
		log.info("ps->{}",ps);
		return pss.selectPapagoStatss(ps);
	}
	@GetMapping("/papago/stats/credat")
	public List<PapagoStatsVO> getPapagoStatsCredat(PapagoStatsVO ps){
		log.info("ps->{}",ps);
		return pss.selectPapagoStatsCredat(ps);
	}
	@GetMapping("/papago/stats/user")
	public List<PapagoStatsVO> getPapagoStatsUser(PapagoStatsVO ps){
		log.info("ps->{}",ps);
		return pss.selectPapagoStatsUser(ps);
	}
}
