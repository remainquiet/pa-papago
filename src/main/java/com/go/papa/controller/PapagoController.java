package com.go.papa.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.go.papa.service.PapagoService;
import com.go.papa.service.PapagoStatsService;
import com.go.papa.vo.Message;
import com.go.papa.vo.PapagoInfoVO;
import com.go.papa.vo.PapagoStatsVO;
import com.go.papa.vo.TransVO;
import com.go.papa.vo.UserInfoVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class PapagoController {
	
	@Resource
	private PapagoService ps;
	@Resource
	PapagoStatsService papagoStatsService;

	@PostMapping("/papago")
	public Message doTranslate(@ModelAttribute TransVO tvo, HttpSession hs) {
		UserInfoVO ui = (UserInfoVO) hs.getAttribute("ui");
		tvo.setUiNum(ui.getUiNum());
		return ps.doTranslate(tvo);
	}
	
	@GetMapping("/papago")
	public List<PapagoInfoVO> getPapagoInfoList(@ModelAttribute PapagoInfoVO pi){
		log.debug("pi={}",pi);
		return ps.getPapagoInfoList(pi);
	}
	@PostMapping("/selectPapagoStatsInfo")
	public List<PapagoStatsVO> getPapagoStatsList(PapagoStatsVO papagoStatsVO){
		return papagoStatsService.selectPapagoStatss(papagoStatsVO);
	}
	
}
