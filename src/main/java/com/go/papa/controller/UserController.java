package com.go.papa.controller;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.go.papa.service.UserService;
import com.go.papa.vo.UserInfoVO;

import lombok.extern.slf4j.Slf4j;

@RestController//view를 보여줄필요가없다 servlet-context를 타지 않는다
@Slf4j
public class UserController {
	
	@Resource //@Resource 같은 경우는 엄밀히 말하자면 이름이 아니라 type으로 찾는다 
	private UserService us;

//	@ResponseBody를 붙여주면 그냥 찍는다 Controller일 때 
	
	@PostMapping("/login")
	public Map<String,Object> doLogin(@RequestBody UserInfoVO ui, HttpSession hs){
		log.info("us=>{}",us);
		log.info("session=>{}",hs);
		log.info("user info=>{}",ui);
		return us.doLogin(ui,hs);
	}
	
}
