package com.go.papa.dao;

import com.go.papa.vo.MessageInfoVO;

public interface MessageInfoDAO {

	public int insertMessageInfo(MessageInfoVO mi);
}
