package com.go.papa.dao;

import java.util.List;

import com.go.papa.vo.PapagoInfoVO;
import com.go.papa.vo.TransVO;

public interface PapagoInfoDAO {
	public List<PapagoInfoVO> selectPapagoInfoList(PapagoInfoVO tvo);
	public PapagoInfoVO selectPapagoInfo(PapagoInfoVO tvo);
	public int insertPapagoInfo(PapagoInfoVO tvo);
	public int updatePapagoInfoForCnt(PapagoInfoVO tvo);
}
