package com.go.papa.dao;

import java.util.List;

import com.go.papa.vo.PapagoStatsVO;

public interface PapagoStatsDAO {

	public List<PapagoStatsVO> selectPapagoStatss(PapagoStatsVO ps);
	public int insertPapagoStats(PapagoStatsVO ps);
	public List<PapagoStatsVO> selectPapagoStatsCredat(PapagoStatsVO ps);
	public List<PapagoStatsVO> selectPapagoStatsUser(PapagoStatsVO ps);
}
