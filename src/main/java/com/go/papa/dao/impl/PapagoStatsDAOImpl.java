package com.go.papa.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.defaults.DefaultSqlSession;
import org.springframework.stereotype.Repository;

import com.go.papa.dao.PapagoStatsDAO;
import com.go.papa.vo.PapagoStatsVO;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class PapagoStatsDAOImpl implements PapagoStatsDAO {

	@Resource
	private SqlSessionFactory ssf;
	
	@Override
	public List<PapagoStatsVO> selectPapagoStatss(PapagoStatsVO ps) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectList("com.go.papa.dao.PapagoStatsMapper.selectPapagoStatss", ps);
		}catch(Exception e){
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return null;
	}

	@Override
	public int insertPapagoStats(PapagoStatsVO ps) {
		SqlSession ss = ssf.openSession();
		try {
			int cnt = ss.insert("com.go.papa.dao.PapagoStatsMapper.insertPapagoStats", ps);
			ss.commit();
			return cnt;
		}catch(Exception e){
			ss.rollback();
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return 0;
	}

	@Override
	public List<PapagoStatsVO> selectPapagoStatsCredat(PapagoStatsVO ps) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectList("com.go.papa.dao.PapagoStatsMapper.selectPapagoStatsCredat", ps);
		}catch(Exception e){
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return null;
	}

	@Override
	public List<PapagoStatsVO> selectPapagoStatsUser(PapagoStatsVO ps) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectList("com.go.papa.dao.PapagoStatsMapper.selectPapagoStatsUser", ps);
		}catch(Exception e){
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return null;
	}

}
