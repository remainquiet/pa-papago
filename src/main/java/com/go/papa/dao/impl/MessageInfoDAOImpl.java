package com.go.papa.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Component;

import com.go.papa.dao.MessageInfoDAO;
import com.go.papa.vo.MessageInfoVO;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class MessageInfoDAOImpl implements MessageInfoDAO {

	@Resource
	private SqlSessionFactory ssf;
	
	@Override
	public int insertMessageInfo(MessageInfoVO mi) {
		
		SqlSession ss = ssf.openSession();
		try {
			int cnt =  ss.insert("com.go.papa.dao.MessageInfoDAO.insertMessageInfo",mi);
			ss.commit();
			return cnt;
		}catch(Exception e){
			ss.rollback();
			log.error(e+"");
		}finally {
			ss.close();
		}

		return 0;
	}

}
