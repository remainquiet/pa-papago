package com.go.papa.dao.impl;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import com.go.papa.dao.UserDAO;
import com.go.papa.vo.UserInfoVO;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class UserDAOImpl implements UserDAO {

	@Resource
	private SqlSessionFactory ssf;

	@Override
	public UserInfoVO doLogin(UserInfoVO ui) {
		SqlSession ss = ssf.openSession();
		try {
			log.info("ui=>{}", ui);
			return ss.selectOne("com.go.papa.dao.UserInfoMapper.doLogin", ui);
		} catch (Exception e) {
			log.error(e.getMessage());
		} finally {
			ss.close();
		}
		return null;
	}

}
