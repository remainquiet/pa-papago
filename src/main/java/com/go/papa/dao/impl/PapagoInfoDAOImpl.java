package com.go.papa.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import com.go.papa.dao.PapagoInfoDAO;
import com.go.papa.vo.PapagoInfoVO;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class PapagoInfoDAOImpl implements PapagoInfoDAO {

	@Resource
	private SqlSessionFactory ssf;

	@Override
	public List<PapagoInfoVO> selectPapagoInfoList(PapagoInfoVO tvo) {
		SqlSession ss = ssf.openSession();
		log.debug("tvo={}", tvo);
		try {
			return ss.selectList("com.go.papa.dao.PapagoInfoDAO.selectPapagoInfos", tvo);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ss.close();
		}
		return null;

	}

	@Override
	public PapagoInfoVO selectPapagoInfo(PapagoInfoVO tvo) {
		SqlSession ss = ssf.openSession();
		log.debug("tvo={}", tvo);
		try {
			return ss.selectOne("com.go.papa.dao.PapagoInfoDAO.selectPapagoInfo", tvo);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ss.close();
		}
		return null;

	}

	@Override
	public int insertPapagoInfo(PapagoInfoVO tvo) {
		SqlSession ss = ssf.openSession();
		try {
			int cnt = ss.insert("com.go.papa.dao.PapagoInfoDAO.insertPapagoInfo", tvo);
			ss.commit();
			return cnt;

		} catch (Exception e) {
			ss.rollback();
			e.printStackTrace();
		} finally {
			ss.close();
		}
		return 0;

	}

	@Override
	public int updatePapagoInfoForCnt(PapagoInfoVO tvo) {
		SqlSession ss = ssf.openSession();
		try {
			int cnt = ss.update("com.go.papa.dao.PapagoInfoDAO.updatePapagoInfoForCnt", tvo);
			ss.commit();
			return cnt;

		} catch (Exception e) {
			ss.rollback();
			e.printStackTrace();
		} finally {
			ss.close();
		}
		return 0;

	}

}
