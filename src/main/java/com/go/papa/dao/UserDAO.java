package com.go.papa.dao;

import com.go.papa.vo.UserInfoVO;

public interface UserDAO {

	public UserInfoVO doLogin(UserInfoVO ui);
}
